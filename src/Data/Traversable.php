<?php
// jshint maxparams: 3

$exports["traverseArrayImpl"] = (function () {
  $array1 = function ($a) {
    return [$a];
  };

  $array2 = function ($a) {
    return function ($b) use (&$a) {
      return [$a, $b];
    };
  };

  $array3 = function ($a) {
    return function ($b) use (&$a) {
      return function ($c) use (&$a, &$b) {
        return [$a, $b, $c];
      };
    };
  };

  $concat2 = function($xs) {
    return function ($ys) use (&$xs) {
      return array_merge($xs, $ys);
    };
  };

  return function ($apply) use (&$array1, &$array2, &$array3, &$concat2) {
    return function ($map) use (&$array1, &$array2, &$array3, &$concat2, &$apply) {
      return function ($pure) use (&$array1, &$array2, &$array3, &$concat2, &$apply, &$map) {
        return function ($f) use (&$array1, &$array2, &$array3, &$concat2, &$apply, &$map, &$pure) {
          return function ($array) use (&$array1, &$array2, &$array3, &$concat2, &$apply, &$map, &$pure, &$f) {
            $go = function ($bot, $top) use (&$array1, &$array2, &$array3, &$concat2, &$apply, &$map, &$pure, &$f, &$array, &$go) {
              switch ($top - $bot) {
              case 0: return $pure([]);
              case 1: return $map($array1)($f($array[$bot]));
              case 2: return $apply($map($array2)($f($array[$bot])))($f($array[$bot + 1]));
              case 3: return $apply($apply($map($array3)($f($array[$bot])))($f($array[$bot + 1])))($f($array[$bot + 2]));
              default:
                // This slightly tricky pivot selection aims to produce two
                // even-length partitions where possible.
                $pivot = $bot + floor(($top - $bot) / 4) * 2;
                return $apply($map($concat2)($go($bot, $pivot)))($go($pivot, $top));
              }
            };
            return $go(0, count($array));
          };
        };
      };
    };
  };
})();
